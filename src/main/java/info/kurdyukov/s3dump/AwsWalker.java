package info.kurdyukov.s3dump;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class AwsWalker implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(AwsWalker.class);
	private static final int PAGE_SIZE = 100;

	private final Stats stats;
	private final AmazonS3 client;
	private final Executor executor;
	private final BlockingQueue<BuffObject> writeQueue;
	private final String bucketName;

	public AwsWalker(Stats stats, AmazonS3 client, Executor executor, BlockingQueue<BuffObject> writeQueue, String bucketName) {
		this.stats = stats;
		this.client = client;
		this.executor = executor;
		this.writeQueue = writeQueue;
		this.bucketName = bucketName;
	}

	@Override
	public void run() {
		try {
			int totalObjectsCount = 0;
			final ListObjectsV2Request req = new ListObjectsV2Request()
					.withBucketName(bucketName)
					.withMaxKeys(PAGE_SIZE);
			ListObjectsV2Result result;
			do {
				result = client.listObjectsV2(req);
				int listedObjects = result.getObjectSummaries().size();
				log.debug("List {} objects from {}", listedObjects, bucketName);
				stats.onList(listedObjects);
				for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
					executor.execute(new AwsReader(stats, client, bucketName, objectSummary.getKey(), writeQueue));
				}
				req.setContinuationToken(result.getNextContinuationToken());
				totalObjectsCount += listedObjects;
			} while (result.isTruncated());
			log.debug("{} objects has bean listed", totalObjectsCount);
		} catch (Exception e) {
			log.error("Couldn't list {} bucket", bucketName, e);
		}
	}
}
