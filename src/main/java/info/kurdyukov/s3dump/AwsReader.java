package info.kurdyukov.s3dump;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AwsReader implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(AwsReader.class);

	private final Stats stats;
	private final AmazonS3 client;
	private final String bucketName;
	private final String objectKey;
	private final BlockingQueue<BuffObject> objectsToWrite;

	public AwsReader(Stats stats, AmazonS3 client, String bucketName, String objectKey,
			BlockingQueue<BuffObject> objectsToWrite) {
		this.stats = stats;
		this.client = client;
		this.bucketName = bucketName;
		this.objectKey = objectKey;
		this.objectsToWrite = objectsToWrite;
	}

	@Override
	public void run() {
		try {
			log.debug("Request {} object from {}", objectKey, bucketName);
			S3Object s3Object = client.getObject(new GetObjectRequest(bucketName, objectKey));
			long length = s3Object.getObjectMetadata().getContentLength();
			if (length < CliApp.MAX_IN_MEMORY_FILE) {
				byte[] buff = IOUtils.toByteArray(s3Object.getObjectContent());
				objectsToWrite.put(new BuffObject(objectKey, buff));
			} else {
				File tempFile = Files.createTempFile("s3dump", String.valueOf(System.currentTimeMillis())).toFile();
				tempFile.deleteOnExit();
				try (OutputStream fos = new FileOutputStream(tempFile)) {
					IOUtils.copy(s3Object.getObjectContent(), fos);
				}
				objectsToWrite.put(new BuffObject(objectKey, tempFile));
			}
			stats.onRead(length);
			log.debug("Read {}bytes for {} object from {}", length, objectKey, bucketName);
		} catch (IOException | InterruptedException e) {
			log.error("Couldn't read {}; bucket = {}", objectKey, bucketName, e);
		}
	}
}
