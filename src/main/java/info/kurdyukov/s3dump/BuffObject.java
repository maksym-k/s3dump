package info.kurdyukov.s3dump;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;

public class BuffObject {

	public static final BuffObject EOS = new BuffObject(null, (byte[]) null);

	private final String path;
	private final byte[] content;
	private final File file;

	private BuffObject(String path, byte[] content, File file) {
		this.path = StringUtils.startsWith("/", path) ? path.substring(1) : path;
		this.content = content;
		this.file = file;
	}

	public BuffObject(String path, byte[] content) {
		this(path, content, null);
	}

	public BuffObject(String path, File file) {
		this(path, null, file);
	}

	public String getPath() {
		return path;
	}

	public long getContentLength() {
		return content != null ? content.length : file.length();
	}

	public InputStream openStream() throws IOException {
		return content != null ? new ByteArrayInputStream(content) : new FileInputStream(file);
	}
}
