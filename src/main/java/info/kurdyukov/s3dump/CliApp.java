package info.kurdyukov.s3dump;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;

public class CliApp {

	public static final long MAX_IN_MEMORY_FILE = 4 * 1024 * 1024;

	private static final Logger log = LoggerFactory.getLogger(CliApp.class);

	private static final Options OPTIONS = new Options();

	static {
		OPTIONS.addOption("d", "dump", false, "Dump S3 bucket");
		OPTIONS.addOption("r", "restore", false, "Restore S3 bucket");

		OPTIONS.addRequiredOption("b", "bucket", true, "ID of AWS Bucket");
		OPTIONS.addRequiredOption("f", "file", true, "Path to output TAR file");
		OPTIONS.addOption("t", "thread", true, "Number of threads for read/write objects from/to AWS");
		OPTIONS.addOption("c", "clean", false, "Clean bucket before restore");
		OPTIONS.addOption("k", "keep", false, "No override exist object during restore");

		OPTIONS.addOption("v", "verbose", false, "Enable Java-style verbose logging");
		OPTIONS.addOption("h", "help", false, "Print this help");
	}

	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();

		try {
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(OPTIONS, args);

			if (cmd.hasOption('h')) {
				printHelp();
				System.exit(1);
			}

			boolean verbose = cmd.hasOption('v');
			if (verbose) {
				ch.qos.logback.classic.Logger appLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(
						"info.kurdyukov.s3dump");
				appLogger.setLevel(Level.TRACE);
			} else {
				ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(
						org.slf4j.Logger.ROOT_LOGGER_NAME);
				rootLogger.setLevel(Level.ERROR);
			}

			int poolSize = Runtime.getRuntime().availableProcessors() * 8;
			if (cmd.hasOption('t')) {
				poolSize = Integer.parseInt(cmd.getOptionValue('t'));
			}

			int queueSize = poolSize * 2;

			Stats stats = new Stats();
			StatusPrinter statPrinter = new StatusPrinter(stats, verbose);
			Thread statPrinterThread = new Thread(statPrinter);
			statPrinterThread.setDaemon(true);
			statPrinterThread.start();

			String bucketName = cmd.getOptionValue('b');
			File tarFile = new File(cmd.getOptionValue('f'));

			if (cmd.hasOption('d')) {
				DumpApp dump = new DumpApp(bucketName, tarFile);
				dump.execute(stats, poolSize, queueSize);
			} else if (cmd.hasOption('r')) {
				RestoreApp restore = new RestoreApp(bucketName, tarFile, cmd.hasOption('c'), cmd.hasOption('k'));
				restore.execute(stats, poolSize, queueSize);
			}

			statPrinter.done();
			statPrinterThread.join();
		} catch (MissingOptionException moe) {
			printHelp();
			System.exit(1);
		} catch (Exception e) {
			log.error("Couldn't load application", e);
			System.exit(-1);
		}

		log.info("Done in {} seconds", TimeUnit.SECONDS.convert(System.currentTimeMillis() - start, TimeUnit.MILLISECONDS));
		System.exit(0);
	}

	private static void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java <-Daws.region=... -Daws.accessKeyId=... -Daws.accessKeyId> "
				+ "-jar s3dump.jar [options]", OPTIONS);
	}
}
