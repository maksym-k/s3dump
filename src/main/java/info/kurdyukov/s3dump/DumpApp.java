package info.kurdyukov.s3dump;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class DumpApp {

	private static final Logger log = LoggerFactory.getLogger(DumpApp.class);

	private final String bucketName;
	private final File tarFile;

	public DumpApp(String bucketName, File tarFile) {
		this.bucketName = bucketName;
		this.tarFile = tarFile;
	}

	public void execute(Stats stats, int poolSize, int queueSize) throws InterruptedException {
		log.debug("Build AWS client");
		AmazonS3 client = AmazonS3ClientBuilder.standard().build();

		try {
			BlockingQueue<BuffObject> writeQueue = new LinkedBlockingQueue<>(queueSize);
			ExecutorService executor = new BlockingExecutor(stats, poolSize, queueSize);

			// start 'list bucket' tasks and pool 'read' tasks to executor
			Thread walker = new Thread(new AwsWalker(stats, client, executor, writeQueue, bucketName));
			walker.start();

			// start 'write TAR' task
			Thread writer = new Thread(new TarWriter(stats, tarFile, writeQueue));
			writer.setPriority(Thread.MAX_PRIORITY);
			writer.start();

			// wait until 'list' thread stop
			walker.join();
			log.debug("Bucket {} has been listed", bucketName);

			// wait until all 'read' tasks will be done
			executor.shutdown();
			executor.awaitTermination(10, TimeUnit.MINUTES);
			log.debug("Read tasks done");

			// wait until TAR will be written
			writeQueue.put(BuffObject.EOS);
			writer.join();
			log.debug("TAR stream crated; size = {}bytes", tarFile.length());
		} finally {
			client.shutdown();
		}
	}
}
