package info.kurdyukov.s3dump;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlockingExecutor extends ThreadPoolExecutor {

	private static final Logger log = LoggerFactory.getLogger(BlockingExecutor.class);

	private final Stats stats;
	private final Semaphore semaphore;

	public BlockingExecutor(Stats stats, int poolSize, int queueSize) {
		super(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
				BlockingExecutor::rejectedExecution);
		this.stats = stats;
		this.semaphore = new Semaphore(queueSize + poolSize - 1);
		prestartAllCoreThreads();
	}

	@Override
	public void execute(Runnable command) {
		try {
			stats.onTaskAdded();
			semaphore.acquire();
			super.execute(command);
		} catch (InterruptedException e) {
			log.error("Attempt take lock object for task queue was interrupted", e);
		}
	}

	@Override
	public Future<?> submit(Runnable task) {
		try {
			stats.onTaskAdded();
			semaphore.acquire();
			return super.submit(task);
		} catch (InterruptedException e) {
			log.error("Attempt take lock object for task queue was interrupted", e);
			throw new IllegalStateException(e);
		}
	}

	@Override
	public <T> Future<T> submit(Runnable task, T result) {
		try {
			stats.onTaskAdded();
			semaphore.acquire();
			return super.submit(task, result);
		} catch (InterruptedException e) {
			log.error("Attempt take lock object for task queue was interrupted", e);
			throw new IllegalStateException(e);
		}
	}

	@Override
	public <T> Future<T> submit(Callable<T> task) {
		try {
			stats.onTaskAdded();
			semaphore.acquire();
			return super.submit(task);
		} catch (InterruptedException e) {
			log.error("Attempt take lock object for task queue was interrupted", e);
			throw new IllegalStateException(e);
		}
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		semaphore.release();
		stats.onTaskCompleted();
	}

	private static void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		log.error("Couldn't accept task '{}'", r);
	}
}
