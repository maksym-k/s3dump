package info.kurdyukov.s3dump;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.concurrent.Executor;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;

public class TarReader implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(TarReader.class);

	private final Stats stats;
	private final AmazonS3 client;
	private final String bucketName;
	private final File tarFile;
	private final Executor executor;
	private final boolean noOverride;

	public TarReader(Stats stats, AmazonS3 client, String bucketName, File tarFile, Executor executor, boolean noOverride) {
		this.stats = stats;
		this.client = client;
		this.bucketName = bucketName;
		this.tarFile = tarFile;
		this.executor = executor;
		this.noOverride = noOverride;
	}

	@Override
	public void run() {
		try (TarArchiveInputStream tarArchive = openTar()) {
			TarArchiveEntry tarEntry = null;
			while ((tarEntry = tarArchive.getNextTarEntry()) != null) {
				stats.onList(1);
				long contentLength = tarEntry.getSize();
				if (contentLength < CliApp.MAX_IN_MEMORY_FILE) {
					byte[] buff = new byte[(int) tarEntry.getSize()];
					IOUtils.read(tarArchive, buff);
					executor.execute(new AwsWriter(stats, client, bucketName, new BuffObject(tarEntry.getName(), buff), noOverride));
				} else {
					File tempFile = Files.createTempFile("s3dump", String.valueOf(System.currentTimeMillis())).toFile();
					tempFile.deleteOnExit();
					try (OutputStream fos = new FileOutputStream(tempFile)) {
						IOUtils.copy(tarArchive, fos);
					}
					executor.execute(new AwsWriter(stats, client, bucketName, new BuffObject(tarEntry.getName(), tempFile), noOverride));
				}
				stats.onRead(contentLength);
			}
		} catch (IOException e) {
			log.error("Couldn't read tar file {}", tarFile);
		}
	}

	private TarArchiveInputStream openTar() throws IOException {
		if (tarFile.getName().toLowerCase().endsWith(".gz")) {
			return new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(tarFile)));
		} else {
			return new TarArchiveInputStream(new FileInputStream(tarFile));
		}
	}
}
