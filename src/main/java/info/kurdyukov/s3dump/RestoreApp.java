package info.kurdyukov.s3dump;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class RestoreApp {

	private static final Logger log = LoggerFactory.getLogger(RestoreApp.class);

	private static final int PAGE_SIZE = 100;

	private final String bucketName;
	private final File tarFile;
	private final boolean cleanBefore;
	private final boolean noOverride;

	public RestoreApp(String bucketName, File tarFile, boolean cleanBefore, boolean noOverride) {
		this.bucketName = bucketName;
		this.tarFile = tarFile;
		this.cleanBefore = cleanBefore;
		this.noOverride = noOverride;
	}

	public void execute(Stats stats, int poolSize, int queueSize) throws InterruptedException {
		log.debug("Build AWS client");
		AmazonS3 client = AmazonS3ClientBuilder.standard().build();

		try {
			if (cleanBefore) {
				deleteAll(client);
			}

			ExecutorService executor = new BlockingExecutor(stats, poolSize, queueSize);

			// start 'read TAR' task: read items from TAR and put it to readQueue
			Thread reader = new Thread(new TarReader(stats, client, bucketName, tarFile, executor, noOverride));
			reader.setPriority(Thread.MAX_PRIORITY);
			reader.start();

			// wait until TAR will be read
			reader.join();
			log.debug("Read {}bytes from TAR stream", tarFile.length());

			// wait until all 'write' tasks will be done
			executor.shutdown();
			executor.awaitTermination(10, TimeUnit.MINUTES);
			log.debug("Write tasks done");
		} finally {
			client.shutdown();
		}
	}

	private void deleteAll(AmazonS3 client) {
		log.debug("Delete all objects in {}", bucketName);
		final ListObjectsV2Request req = new ListObjectsV2Request()
				.withBucketName(bucketName)
				.withMaxKeys(PAGE_SIZE);
		long totalObjectsCount = 0;
		ListObjectsV2Result result;
		do {
			result = client.listObjectsV2(req);
			if (!result.getObjectSummaries().isEmpty()) {
				List<String> objecKeys = result.getObjectSummaries().stream()
						.map(S3ObjectSummary::getKey)
						.collect(Collectors.toList());
				client.deleteObjects(new DeleteObjectsRequest(bucketName)
						.withKeys(objecKeys.toArray(new String[objecKeys.size()])));
				totalObjectsCount += objecKeys.size();
			}
			req.setContinuationToken(result.getNextContinuationToken());
		} while (result.isTruncated());
		log.debug("Deleted {} objects from {}", totalObjectsCount, bucketName);
	}
}
