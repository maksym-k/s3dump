package info.kurdyukov.s3dump;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatusPrinter implements Runnable {

	private static final int LISTED_COLUMN_WIDTH = 10; // 9,999,999,999 objects
	private static final int TASKS_COLUMN_WIDTH = 10; // 999 tasks
	private static final int READ_COLUMN_WIDTH = 8; // rounded to nnn.m (G|M|K)bytes
	private static final int WRITE_COLUMN_WIDTH = 8;

	private static final long PRINT_PERIOD = 1000L;

	private static final Logger log = LoggerFactory.getLogger(StatusPrinter.class);

	private final Stats stats;
	private final AtomicBoolean done = new AtomicBoolean(false);
	private final boolean verbose;

	public StatusPrinter(Stats stats, boolean verbose) {
		this.stats = stats;
		this.verbose = verbose;
	}

	public void done() {
		done.set(true);
	}

	@Override
	public void run() {
		while (!done.get()) {
			try {
				if (verbose) {
					log.info(status());
				} else {
					System.out.print(status() + "\r");
				}
				Thread.sleep(PRINT_PERIOD);
			} catch (InterruptedException ignore) {
				break;
			}
		}
	}

	private String status() {
		StringBuilder buff = new StringBuilder();

		buff.append("Tasks in queue: ");
		printWithAlign(buff, String.valueOf(stats.getPendingTasks()), TASKS_COLUMN_WIDTH);

		buff.append(" | Listed objects: ");
		printWithAlign(buff, String.valueOf(stats.getListedObjects()), LISTED_COLUMN_WIDTH);

		buff.append(" | Read: ");
		printWithAlign(buff, FileUtils.byteCountToDisplaySize(stats.getReadBytes()), READ_COLUMN_WIDTH);

		buff.append(" | Write: ");
		printWithAlign(buff, FileUtils.byteCountToDisplaySize(stats.getReadBytes()), WRITE_COLUMN_WIDTH);

		return buff.toString();
	}

	private void printWithAlign(StringBuilder buff, String value, int width) {
		buff.append(StringUtils.repeat(' ', width - value.length())).append(value);
	}
}
