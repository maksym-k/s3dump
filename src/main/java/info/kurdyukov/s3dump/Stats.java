package info.kurdyukov.s3dump;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Stats {

	private final AtomicInteger listedObjects = new AtomicInteger();
	private final AtomicLong readBytes = new AtomicLong();
	private final AtomicLong writeBytes = new AtomicLong();
	private final AtomicInteger pendingTasks = new AtomicInteger();

	public void onList(int listedcount) {
		listedObjects.getAndAdd(listedcount);
	}

	public void onRead(long bytes) {
		readBytes.getAndAdd(bytes);
	}

	public void onWrite(long bytes) {
		writeBytes.getAndAdd(bytes);
	}

	public void onTaskAdded() {
		pendingTasks.incrementAndGet();
	}

	public void onTaskCompleted() {
		pendingTasks.decrementAndGet();
	}

	public int getListedObjects() {
		return listedObjects.get();
	}

	public long getReadBytes() {
		return readBytes.get();
	}

	public long getWriteBytes() {
		return writeBytes.get();
	}

	public int getPendingTasks() {
		return pendingTasks.get();
	}
}
