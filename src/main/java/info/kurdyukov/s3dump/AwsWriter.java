package info.kurdyukov.s3dump;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;

public class AwsWriter implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(AwsWriter.class);

	private final Stats stats;
	private final AmazonS3 client;
	private final String bucketName;
	private final BuffObject object;
	private final boolean noOverride;

	public AwsWriter(Stats stats, AmazonS3 client, String bucketName, BuffObject object, boolean noOverride) {
		this.stats = stats;
		this.client = client;
		this.bucketName = bucketName;
		this.object = object;
		this.noOverride = noOverride;
	}

	@Override
	public void run() {
		String objectKey = object.getPath();
		long contentLength = object.getContentLength();
		log.debug("Put object {} to {}", objectKey, bucketName);

		try {
			if (noOverride && client.doesObjectExist(objectKey, objectKey)) {
				log.debug("Skip overriding of object {} in {}", objectKey, bucketName);
				return;
			}

			ObjectMetadata s3Meta = new ObjectMetadata();
			s3Meta.setContentLength(contentLength);
			try (InputStream is = object.openStream()) {
				client.putObject(bucketName, objectKey, is, s3Meta);
			}
			stats.onWrite(contentLength);
			log.debug("Write {}bytes for {}", contentLength, objectKey);
		} catch (AmazonClientException | IOException e) {
			log.error("Couldn't write {}; bucket = {}", objectKey, bucketName, e);
		}
	}
}
