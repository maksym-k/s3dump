package info.kurdyukov.s3dump;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TarWriter implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(TarWriter.class);

	private final Stats stats;
	private final File output;
	private final BlockingQueue<BuffObject> readyObjects;

	public TarWriter(Stats stats, File output, BlockingQueue<BuffObject> readyObjects) {
		this.stats = stats;
		this.output = output;
		this.readyObjects = readyObjects;
	}

	@Override
	public void run() {
		log.debug("Open TAR stream to {}", output);
		try (TarArchiveOutputStream tarOutput = openTar()) {
			BuffObject obj = null;
			while ((obj = readyObjects.take()) != null && obj != BuffObject.EOS) {
				try (InputStream is = obj.openStream()) {
					putContent(tarOutput, obj.getPath(), is, obj.getContentLength());
				}
				stats.onWrite(obj.getContentLength());
			}
		} catch (IOException | InterruptedException e) {
			log.error("Couldn't write {}", output, e);
		}
	}

	private void putContent(TarArchiveOutputStream tarOutput, String name, InputStream is, long contentLength) throws IOException {
		log.debug("Open entry {}", name);
		TarArchiveEntry entry = new TarArchiveEntry(name);
		entry.setSize(contentLength);
		tarOutput.putArchiveEntry(entry);
		IOUtils.copy(is, tarOutput);
		tarOutput.closeArchiveEntry();
	}

	private TarArchiveOutputStream openTar() throws IOException {
		if (output.getName().toLowerCase().endsWith(".gz")) {
			return new TarArchiveOutputStream(new GzipCompressorOutputStream(new FileOutputStream(output)));
		} else {
			return new TarArchiveOutputStream(new FileOutputStream(output));
		}
	}
}
